#include "receiver.hpp"

#include <cstring>

#include <iomanip>

#include <common/clock.h>

#include <common/lout.hpp>
#include <common/packet.hpp>

receiver::receiver(common::processor& p, const boost::asio::ip::tcp::endpoint& endpoint)
    : processor_ {p}
    , io_context_ {}
    , socket_ {io_context_}
    , listen_endpoint_ {endpoint}
    , read_size_ {0}
{
}

receiver::~receiver()
{
}

void receiver::run()
{
    common::lout() << "Waiting for TCP connection on " << listen_endpoint_ << std::endl;

    boost::asio::signal_set signal_set {io_context_};
    signal_set.add(SIGINT);
    signal_set.add(SIGTERM);
    signal_set.async_wait([this]([[maybe_unused]] const auto& ec, [[maybe_unused]] auto signal) {
        io_context_.stop();
    });

    boost::asio::ip::tcp::acceptor acceptor {io_context_, listen_endpoint_};

    acceptor.async_accept(socket_,
                          [this, &acceptor](auto ec) {
                              if (ec)
                              {
                                  common::lout() << "Error accept connection: " << ec.message() << std::endl;
                                  io_context_.stop();
                                  return;
                              }
                              common::lout() << "Connection accepted from: " << socket_.remote_endpoint() << std::endl;
                              acceptor.close();
                              read_next();
                          });

    io_context_.run();

    common::lout() << "Normal shutdown." << std::endl;
}

void receiver::read_next()
{
    socket_.async_read_some(boost::asio::buffer(buffer_.data() + read_size_, buffer_.size() - read_size_),
                            [this](const auto& ec, auto bytes_transferred) {
                                if (ec)
                                {
                                    if (boost::system::errc::no_such_file_or_directory == ec.value())
                                    {
                                        common::lout() << "Connection lost." << std::endl;
                                    }
                                    else
                                    {
                                        common::lout() << "Error receiving: " << ec.message() << std::endl;
                                    }
                                    socket_.close();
                                    io_context_.stop();
                                    return;
                                };
                                read_size_ += bytes_transferred;
                                process_data();
                                read_next();
                            });
}

void receiver::process_data()
{
    while (true)
    {
        if (2 > read_size_)
        {
            return;
        }

        std::size_t packet_size {static_cast<std::size_t>(buffer_[0]) + (static_cast<std::size_t>(buffer_[1]) << 8)};
        if ((packet_size + 2) > read_size_)
        {
            return;
        }

        common::packet packet {buffer_.data() + 2, packet_size};
        if (!packet)
        {
            common::lout() << "Received " << (packet_size + 2)
                           << " byte(s) which cannot be treated as valid packet." << std::endl;
        }
        else
        {
            char time_buffer[MILLISECONDS_BUFFER_SIZE];
            common::lout() << "Received: #" << std::setw(5) << std::setfill('0') << packet.packet_number()
                           << ", #" << ::milliseconds_to_string(time_buffer, packet.receive_timestamp())
                           << ", Checksum: " << (packet.is_checksum_valid() ? "PASS" : "FAIL")
                           << std::endl;
            processor_.enqueue(std::move(packet), true);
        }

        packet_size += 2;
        if (packet_size < read_size_)
        {
            std::memmove(buffer_.data(), buffer_.data() + packet_size, read_size_ - packet_size);
        }
        read_size_ -= packet_size;
    }
}
