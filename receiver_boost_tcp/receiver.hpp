#ifndef ITECO_BOOST_UDP_RECEIVER_HPP
#define ITECO_BOOST_UDP_RECEIVER_HPP

#include <array>

#include <boost/asio.hpp>

#include <common/processor.hpp>

class receiver
{
public:
    receiver(common::processor& p, const boost::asio::ip::tcp::endpoint& endpoint);
    ~receiver();

public:
    void run();

private:
    void read_next();
    void process_data();

private:
    common::processor& processor_;
    boost::asio::io_context io_context_;
    boost::asio::ip::tcp::socket socket_;
    boost::asio::ip::tcp::endpoint listen_endpoint_;
    std::array<uint8_t, 0x10000 + 2> buffer_;
    std::size_t read_size_;
};

#endif // ITECO_BOOST_UDP_RECEIVER_HPP
