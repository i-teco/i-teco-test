﻿#include <cstdint>
#include <cstdlib>

#include <chrono>
#include <iomanip>
#include <iostream>
#include <random>
#include <thread>

#include <boost/asio.hpp>
#include <common/packet.hpp>
#include <common/ticker.hpp>

int main(int argc, char** argv)
{
    if (3 != argc)
    {
        std::cout << "Usage:" << std::endl
                  << argv[0] << " <address_to_send> <port_to_send>" << std::endl;
        return 1;
    }

    boost::system::error_code ec {};
    auto addr {boost::asio::ip::make_address(argv[1], ec)};
    if (ec)
    {
        std::cerr << "Invalid address: " << argv[1] << " (" << ec.message() << ")" << std::endl;
        return 1;
    }
    auto port {std::strtoul(argv[2], nullptr, 10)};
    if ((0 == port) || (0xFFFF < port))
    {
        std::cerr << "Invalid port: " << argv[2] << std::endl;
        return 1;
    }
    boost::asio::ip::tcp::endpoint endpoint {addr, static_cast<uint16_t>(port)};

    boost::asio::io_context io_context;
    boost::asio::ip::tcp::socket socket {io_context};
    std::cout << "Connecting to " << endpoint << "..." << std::endl;

    socket.connect(endpoint, ec);
    if (ec)
    {
        std::cerr << "Error establishing connection: " << ec.message() << ")" << std::endl;
        return 1;
    }

    std::cout << "Sending " << NUMBER_OF_CHUNKS << " chunk(s) of " << PACKETS_PER_CHUNK << " packet(s)" << std::endl
              << "Interval between packets : " << INTERPACKET_INTERVAL << " milliseconds" << std::endl
              << "Interval between chunks  : " << INTERCHUNK_INTERVAL << " milliseconds" << std::endl;

    std::random_device rd {};
    std::default_random_engine re {rd()};
    std::uniform_int_distribution<std::remove_reference_t<decltype(*::get_packet_words(nullptr))>> word_dist {};
    std::uniform_int_distribution<std::size_t> word_count_dist {PACKET_MIN_WORDS_COUNT, PACKET_MAX_WORDS_COUNT};

    common::ticker ticker {};

    for (std::size_t i = 0; i < NUMBER_OF_CHUNKS * PACKETS_PER_CHUNK; ++i)
    {
        common::packet packet {static_cast<unsigned>(i), word_count_dist(re)};
        for (std::size_t j = 0; j < packet.words_count(); ++j)
        {
            packet.word(j) = word_dist(re);
        }

        ticker.wait_next_time_point((0 == (i % PACKETS_PER_CHUNK)) ? INTERCHUNK_INTERVAL : INTERPACKET_INTERVAL);

        auto data_size {packet.data_size()};
        const uint8_t size_to_send[2] = {static_cast<uint8_t>(data_size), static_cast<uint8_t>(data_size >> 8)};

        std::array<boost::asio::const_buffer, 2> buffers {
            boost::asio::buffer(size_to_send),
            boost::asio::buffer(packet.data(), data_size)};

        std::size_t written {socket.write_some(buffers, ec)};
        milliseconds_time_t after_send_ts {::get_milliseconds_time()};
        if (ec)
        {
            std::cerr << "Error write packet: " << ec.message() << std::endl;
            return 1;
        }
        if ((data_size + sizeof(size_to_send)) != written)
        {
            std::cerr << "Not all data written (" << written << " of "
                      << (data_size + sizeof(size_to_send)) << ")" << std::endl;
            return 1;
        }

        char time_buffer[MILLISECONDS_BUFFER_SIZE];
        std::cout << "Sent: #" << std::setw(5) << std::setfill('0') << packet.packet_number()
                  << ", #" << ::milliseconds_to_string(time_buffer, packet.send_timestamp())
                  << ", Sent time: " << std::setw(4) << std::setfill(' ') << (after_send_ts - packet.send_timestamp())
                  << ", Jitter: " << std::setw(5) << std::setfill(' ') << ticker.current_jitter()
                  << std::endl;
    }

    return 0;
}
