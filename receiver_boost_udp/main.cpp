﻿#include <cstdlib>

#include <iomanip>
#include <iostream>
#include <string>
#include <thread>

#include <boost/asio.hpp>
#include <common/lout.hpp>
#include <common/processor.hpp>

#include "receiver.hpp"

int main(int argc, char** argv)
{
    if (3 != argc)
    {
        std::cout << "Usage:" << std::endl
                  << argv[0] << " <address_to_bind> <port_to_bind>" << std::endl
                  << "<address_to_bind> can be '*' for listen on all IPv4 addresses." << std::endl;
        return 1;
    }

    boost::asio::ip::address addr {boost::asio::ip::address_v4::any()};
    if (std::string {argv[1]} != "*")
    {
        boost::system::error_code ec {};
        addr = boost::asio::ip::make_address(argv[1], ec);
        if (ec)
        {
            std::cerr << "Invalid address: " << argv[1] << " (" << ec.message() << ")" << std::endl;
            return 1;
        }
    }
    auto port {std::strtoul(argv[2], nullptr, 10)};
    if ((0 == port) || (0xFFFF < port))
    {
        std::cerr << "Invalid port: " << argv[2] << std::endl;
        return 1;
    }

    try
    {
        common::processor
            p {CYCLIC_BUFFER_SIZE, [](const common::packet& packet) {
                   std::this_thread::sleep_for(std::chrono::milliseconds(PACKET_PROCESSING_TIME));
                   char time_buffer[MILLISECONDS_BUFFER_SIZE];
                   common::lout() << "Processed: #" << std::setw(5) << std::setfill('0') << packet.packet_number()
                                  << ", #" << ::milliseconds_to_string(time_buffer, get_milliseconds_time())
                                  << ", Checksum: " << (packet.is_checksum_valid() ? "PASS" : "FAIL")
                                  << std::endl;
               }};
        p.start();

        receiver r {p, boost::asio::ip::udp::endpoint {addr, static_cast<uint16_t>(port)}};
        r.run();

        p.stop();
        // TODO: Output statistics
    }
    catch (const std::exception& e)
    {
        std::cerr << "Exception while running: " << e.what() << std::endl;
        return 1;
    }
    catch (...)
    {
        std::cerr << "Unknown exception while running" << std::endl;
        return 1;
    }

    return 0;
}
