#ifndef ITECO_BOOST_UDP_RECEIVER_HPP
#define ITECO_BOOST_UDP_RECEIVER_HPP

#include <array>

#include <boost/asio.hpp>

#include <common/processor.hpp>

class receiver
{
public:
    receiver(common::processor& p, const boost::asio::ip::udp::endpoint& endpoint);
    ~receiver();

public:
    void run();

private:
    void read_next();

private:
    common::processor& processor_;
    boost::asio::io_context io_context_;
    boost::asio::ip::udp::socket socket_;
    boost::asio::ip::udp::endpoint sender_endpoint_;
    std::array<char, 0x10000> buffer_; // UDP max size
};

#endif // ITECO_BOOST_UDP_RECEIVER_HPP
