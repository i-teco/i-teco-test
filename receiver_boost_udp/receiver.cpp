#include "receiver.hpp"

#include <iomanip>

#include <common/clock.h>

#include <common/lout.hpp>
#include <common/packet.hpp>

receiver::receiver(common::processor& p, const boost::asio::ip::udp::endpoint& endpoint)
    : processor_ {p}
    , io_context_ {}
    , socket_ {io_context_, endpoint}
{
}

receiver::~receiver()
{
}

void receiver::run()
{
    common::lout() << "Waiting for packets on " << socket_.local_endpoint() << std::endl;

    boost::asio::signal_set signal_set {io_context_};
    signal_set.add(SIGINT);
    signal_set.add(SIGTERM);
    signal_set.async_wait([this]([[maybe_unused]] const auto& ec, [[maybe_unused]] auto signal) {
        io_context_.stop();
    });

    read_next();
    io_context_.run();

    common::lout() << "Normal shutdown." << std::endl;
}

void receiver::read_next()
{
    socket_.async_receive_from(boost::asio::buffer(buffer_), sender_endpoint_, [this](const auto& ec, auto bytes_transferred) {
        if (ec)
        {
            common::lout() << "Error receiving: " << ec.message() << std::endl;
            io_context_.stop();
            return;
        };
        common::packet packet {buffer_.data(), bytes_transferred};
        if (!packet)
        {
            common::lout() << "Received " << bytes_transferred << " byte(s) from " << sender_endpoint_
                           << " which cannot be treated as valid packet." << std::endl;
        }
        else
        {
            char time_buffer[MILLISECONDS_BUFFER_SIZE];
            common::lout() << "Received: #" << std::setw(5) << std::setfill('0') << packet.packet_number()
                           << ", #" << ::milliseconds_to_string(time_buffer, packet.receive_timestamp())
                           << ", Checksum: " << (packet.is_checksum_valid() ? "PASS" : "FAIL")
                           << std::endl;
            processor_.enqueue(std::move(packet), true);
        }
        read_next();
    });
}
