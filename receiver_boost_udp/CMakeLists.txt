cmake_minimum_required(VERSION 3.17)

project(receiver_boost_udp
    LANGUAGES CXX
    )

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
find_package(Boost 1.67 REQUIRED)

add_executable(${PROJECT_NAME}
    main.cpp
    receiver.hpp
    receiver.cpp
)

set_target_properties(${PROJECT_NAME} PROPERTIES
    CXX_STANDARD 17
    CXX_STANDARD_REQUIRED ON
    CXX_EXTENSIONS OFF
    )

target_link_libraries(${PROJECT_NAME}
    Common::Common
    Boost::boost
    Threads::Threads
    )

target_compile_definitions(${PROJECT_NAME} PRIVATE
    ${PARAMS_COMPILE_DEFINITIONS}
    )
