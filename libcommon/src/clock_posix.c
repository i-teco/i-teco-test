#include "common/clock.h"

#include <sys/time.h>

milliseconds_time_t get_milliseconds_time()
{
    struct timeval tv;
    gettimeofday(&tv, 0);
    return (((milliseconds_time_t)tv.tv_sec) * 1000ull) + (((milliseconds_time_t)tv.tv_usec) / 1000ull);
}
