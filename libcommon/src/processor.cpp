#include "common/processor.hpp"

namespace common
{
processor::processor(size_t queue_size, handler_t handler)
    : handler_ {std::move(handler)}
    , queue_ {queue_size}
    , write_index_ {0}
    , read_index_ {0}
{
}

processor::~processor()
{
    stop_thread();
}

void processor::start()
{
    if (thread_.joinable())
    {
        return;
    }
    thread_ = std::thread(&processor::pthread_proc, this);
}

void processor::stop()
{
    stop_thread();
    // TODO: Calculate statistics
}

void processor::enqueue(common::packet packet, [[maybe_unused]] bool force)
{
    std::lock_guard lock {queue_mutex_};
    queue_[(write_index_++) % queue_.size()] = std::move(packet);
    queue_cond_.notify_one();
}

void processor::stop_thread()
{
    if (thread_.joinable())
    {
        enqueue(common::packet {}, true);
        thread_.join();
        thread_ = std::thread {};
    }
}

void processor::pthread_proc()
{
    while (true)
    {
        std::unique_lock lock {queue_mutex_};
        queue_cond_.wait(lock, [this] {
            return (write_index_ > read_index_);
        });
        if ((read_index_ + queue_.size()) < write_index_)
        {
            read_index_ = write_index_ - queue_.size();
        }
        common::packet packet {std::move(queue_[read_index_ % queue_.size()])};
        queue_[(read_index_++) % queue_.size()] = common::packet {};
        lock.unlock();
        if (!packet)
        {
            break;
        }
        handler_(packet);
    }
}
} // namespace common
