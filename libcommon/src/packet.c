#include "common/packet.h"

#include <stdlib.h>
#include <string.h>

#include "common/clock.h"
#include "common/md5.h"

struct packet_t
{
    unsigned packet_number_;
    unsigned flags_;
    milliseconds_time_t send_timestamp_;
    milliseconds_time_t receive_timestamp_;
    uint8_t* serialized_data_;
    size_t words_count_;
};

#define PACKET_FLAG_READ_ONLY (0x1)
#define PACKET_FLAG_MD5_VALID (0x2)

struct packet_t* packet_create(unsigned packet_number, size_t words_count)
{
    if (PACKET_MAX_PACKET_NUMBER < packet_number)
    {
        return 0;
    }

    if ((PACKET_MIN_WORDS_COUNT > words_count) || (PACKET_MAX_WORDS_COUNT < words_count))
    {
        return 0;
    }

    struct packet_t* packet = (struct packet_t*)calloc(1, sizeof(struct packet_t) + (sizeof(uint16_t) * words_count));
    packet->packet_number_ = packet_number;
    packet->words_count_ = words_count;

    return packet;
}

void packet_free(struct packet_t* packet)
{
    if (0 != packet)
    {
        if (0 != packet->serialized_data_)
        {
            free(packet->serialized_data_);
        }
        free(packet);
    }
}

unsigned get_packet_packet_number(const struct packet_t* packet)
{
    return packet->packet_number_;
}

size_t get_packet_words_count(const struct packet_t* packet)
{
    return packet->words_count_;
}

uint16_t* get_packet_words(struct packet_t* packet)
{
    if ((0 == packet) || (0 != (packet->flags_ & PACKET_FLAG_READ_ONLY)))
    {
        return 0;
    }
    return (uint16_t*)(packet + 1);
}

const uint16_t* get_packet_words_const(const struct packet_t* packet)
{
    if (0 == packet)
    {
        return 0;
    }
    return (const uint16_t*)(packet + 1);
}

milliseconds_time_t get_packet_send_timestamp(const struct packet_t* packet)
{
    if (0 == packet)
    {
        return 0;
    }
    return packet->send_timestamp_;
}

milliseconds_time_t get_packet_receive_timestamp(const struct packet_t* packet)
{
    if (0 == packet)
    {
        return 0;
    }
    return packet->receive_timestamp_;
}

int packet_is_read_only(const struct packet_t* packet)
{
    if (0 == packet)
    {
        return 0;
    }
    return (0 != (packet->flags_ & PACKET_FLAG_READ_ONLY));
}

int packet_is_checksum_valid(const struct packet_t* packet)
{
    if (0 == packet)
    {
        return 0;
    }
    return (0 != (packet->flags_ & PACKET_FLAG_MD5_VALID));
}

/* Serialization format
   ============================================================
   Offset                  \ Value           \ Size
   ------------------------+-----------------+-----------------
   0:                      \ <packet number> \ 2
   2:                      \ <send_time>     \ 8
   18:                     \ <words>         \ <words_count> * 2
   <words_count> * 2 + 10: \ <md5_hash>      \ 16
   ------------------------+-----------------+-----------------
   Total: <words_count> * 2 + 26
   ============================================================
   All multibyte values are serialized with little-endian format
   Words counts is calculated from total packet size
*/

static inline void store_le(uint8_t** data, uint64_t value, size_t size)
{
    while (size--)
    {
        *(((*data)++)) = (uint8_t)value;
        value >>= 8;
    }
}

static inline uint64_t load_le(const uint8_t** data, size_t size)
{
    uint64_t value = 0;
    for (size_t i = 0; i < size; ++i)
    {
        value += (((uint64_t)(*((*data)++))) << (i * 8));
    }
    return value;
}

size_t get_packet_data_size(const struct packet_t* packet)
{
    if (0 == packet)
    {
        return 0;
    }
    return (packet->words_count_ * 2) + 26;
}

const void* get_packet_data(struct packet_t* packet)
{
    if ((0 == packet) || (0 != (packet->flags_ & PACKET_FLAG_READ_ONLY)))
    {
        return 0;
    }

    if (0 == packet->serialized_data_)
    {
        packet->serialized_data_ = (uint8_t*)calloc(1, get_packet_data_size(packet));
    }

    packet->send_timestamp_ = get_milliseconds_time();

    uint8_t* data = packet->serialized_data_;
    store_le(&data, packet->packet_number_, 2);
    store_le(&data, packet->send_timestamp_, 8);

    uint16_t* words = (uint16_t*)(packet + 1);
    for (size_t i = 0; i < packet->words_count_; ++i)
    {
        store_le(&data, words[i], 2);
    }

    md5_from_block(data, words, packet->words_count_ * 2);

    return packet->serialized_data_;
}

struct packet_t* packet_load(const void* data, size_t size)
{
    milliseconds_time_t receive_timestamp = get_milliseconds_time();

    if (0 == data)
    {
        return 0;
    }

    if ((0 != (size % 2))
        || (((PACKET_MIN_WORDS_COUNT * 2) + 26) > size)
        || (((PACKET_MAX_WORDS_COUNT * 2) + 26) < size))
    {
        return 0;
    }

    size_t words_count = (size - 26) / 2;
    struct packet_t* packet = (struct packet_t*)calloc(1, sizeof(struct packet_t) + (sizeof(uint16_t) * words_count));
    packet->words_count_ = words_count;

    const uint8_t* udata = (const uint8_t*)data;
    packet->packet_number_ = load_le(&udata, 2);
    packet->send_timestamp_ = load_le(&udata, 8);

    uint16_t* words = (uint16_t*)(packet + 1);
    for (size_t i = 0; i < packet->words_count_; ++i)
    {
        words[i] = load_le(&udata, 2);
    }

    uint8_t md5[MD5_SIZE];
    md5_from_block(md5, words, packet->words_count_ * 2);

    if (0 == memcmp(udata, md5, MD5_SIZE))
    {
        packet->flags_ |= PACKET_FLAG_MD5_VALID;
    }
    packet->flags_ |= PACKET_FLAG_READ_ONLY;

    packet->receive_timestamp_ = receive_timestamp;
    return packet;
}
