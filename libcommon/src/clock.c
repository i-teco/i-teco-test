#include "common/clock.h"

#include <stdio.h>
#include <time.h>

char* milliseconds_to_string(char* buffer, milliseconds_time_t t)
{
    time_t seconds = t / 1000ull;
    unsigned milliseconds = t % 1000ul;
    struct tm tm = *gmtime(&seconds);

    sprintf(buffer, "%04u/%02u/%02u %02u:%02u:%02u.%03u",
            tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, milliseconds);
    return buffer;
}
