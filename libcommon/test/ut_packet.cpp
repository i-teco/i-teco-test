﻿#include <gtest/gtest.h>

#include <algorithm>
#include <iterator>
#include <random>
#include <vector>

#include <common/packet.h>
#include <common/packet.hpp>

TEST(Common_Packet, Create_Destroy_C)
{
    {
        auto* packet {packet_create(1, PACKET_MIN_WORDS_COUNT)};
        ASSERT_NE(packet, nullptr);
        EXPECT_EQ(get_packet_packet_number(packet), 1);
        EXPECT_EQ(get_packet_words_count(packet), PACKET_MIN_WORDS_COUNT);
        packet_free(packet);
    }
    {
        auto* packet {packet_create(PACKET_MAX_PACKET_NUMBER, PACKET_MAX_WORDS_COUNT)};
        ASSERT_NE(packet, nullptr);
        EXPECT_EQ(get_packet_packet_number(packet), PACKET_MAX_PACKET_NUMBER);
        EXPECT_EQ(get_packet_words_count(packet), PACKET_MAX_WORDS_COUNT);
        packet_free(packet);
    }
    {
        auto* packet {packet_create(PACKET_MAX_PACKET_NUMBER + 1, PACKET_MIN_WORDS_COUNT)};
        EXPECT_EQ(packet, nullptr);
    }
    {
        auto* packet {packet_create(PACKET_MAX_PACKET_NUMBER, PACKET_MIN_WORDS_COUNT - 1)};
        EXPECT_EQ(packet, nullptr);
    }
    {
        auto* packet {packet_create(PACKET_MAX_PACKET_NUMBER, PACKET_MAX_WORDS_COUNT + 1)};
        EXPECT_EQ(packet, nullptr);
    }
}

TEST(Common_Packet, Create_Destroy_CPP)
{
    {
        common::packet packet {1, PACKET_MIN_WORDS_COUNT};
        ASSERT_TRUE(packet);
        EXPECT_EQ(packet.packet_number(), 1);
        EXPECT_EQ(packet.words_count(), PACKET_MIN_WORDS_COUNT);
    }
    {
        common::packet packet {PACKET_MAX_PACKET_NUMBER, PACKET_MAX_WORDS_COUNT};
        ASSERT_TRUE(packet);
        EXPECT_EQ(packet.packet_number(), PACKET_MAX_PACKET_NUMBER);
        EXPECT_EQ(packet.words_count(), PACKET_MAX_WORDS_COUNT);
    }
    {
        common::packet packet {PACKET_MAX_PACKET_NUMBER + 1, PACKET_MIN_WORDS_COUNT};
        EXPECT_FALSE(packet);
    }
    {
        common::packet packet {PACKET_MAX_PACKET_NUMBER, PACKET_MIN_WORDS_COUNT - 1};
        EXPECT_FALSE(packet);
    }
    {
        common::packet packet {PACKET_MAX_PACKET_NUMBER, PACKET_MAX_WORDS_COUNT + 1};
        EXPECT_FALSE(packet);
    }
}

TEST(Common_Packet, Data_Access_C)
{
    enum : std::size_t
    {
        OFFSET = 0x1234,
    };

    using word_t = std::remove_reference_t<decltype(*get_packet_words(nullptr))>;

    {
        auto* packet {packet_create(1, PACKET_MAX_WORDS_COUNT)};
        ASSERT_NE(packet, nullptr);
        EXPECT_EQ(get_packet_packet_number(packet), 1);
        EXPECT_EQ(get_packet_words_count(packet), PACKET_MAX_WORDS_COUNT);

        auto* write {::get_packet_words(packet)};
        for (std::size_t i = 0; i < PACKET_MAX_WORDS_COUNT; ++i)
        {
            *(write++) = static_cast<word_t>(i + OFFSET);
        }

        const auto* read {::get_packet_words_const(packet)};
        for (std::size_t i = 0; i < PACKET_MAX_WORDS_COUNT; ++i)
        {
            EXPECT_EQ(static_cast<std::size_t>(read[i]), i + OFFSET);
        }

        packet_free(packet);
    }
}

TEST(Common_Packet, Data_Access_CPP)
{
    enum : std::size_t
    {
        OFFSET = 0x4321,
    };

    using word_t = std::remove_reference_t<decltype(*get_packet_words(nullptr))>;

    {
        common::packet packet {1, PACKET_MAX_WORDS_COUNT};
        ASSERT_TRUE(packet);
        EXPECT_EQ(packet.packet_number(), 1);
        EXPECT_EQ(packet.words_count(), PACKET_MAX_WORDS_COUNT);

        for (std::size_t i = 0; i < PACKET_MAX_WORDS_COUNT; ++i)
        {
            packet.words()[i] = static_cast<word_t>(i + OFFSET);
        }

        const auto& read_buf {packet};
        for (std::size_t i = 0; i < PACKET_MAX_WORDS_COUNT; ++i)
        {
            EXPECT_EQ(static_cast<std::size_t>(read_buf.words()[i]), i + OFFSET);
        }
    }

    {
        common::packet packet {static_cast<unsigned>(0), PACKET_MAX_WORDS_COUNT};
        ASSERT_TRUE(packet);
        EXPECT_EQ(packet.packet_number(), 0);
        EXPECT_EQ(packet.words_count(), PACKET_MAX_WORDS_COUNT);

        for (std::size_t i = 0; i < packet.words_count(); ++i)
        {
            packet.word(i) = static_cast<word_t>(i + OFFSET);
        }

        const auto& read_buf {packet};
        for (std::size_t i = 0; i < packet.words_count(); ++i)
        {
            EXPECT_EQ(static_cast<std::size_t>(read_buf.word(i)), i + OFFSET);
        }
    }
}

TEST(Common_Packet, Deserialize_Wrong_Size)
{
    const auto min_size {common::packet {1, PACKET_MIN_WORDS_COUNT}.data_size()};
    const auto max_size {common::packet {1, PACKET_MAX_WORDS_COUNT}.data_size()};
    ASSERT_GT(min_size, 2);
    ASSERT_GT(max_size, 2);
    ASSERT_GT(max_size, min_size);

    std::vector<char> data(max_size + 2, 0);
    EXPECT_FALSE((common::packet {data.data(), min_size - 2}));
    EXPECT_FALSE((common::packet {data.data(), max_size + 2}));
    EXPECT_FALSE((common::packet {data.data(), min_size + 1}));
    EXPECT_FALSE((common::packet {data.data(), max_size - 1}));
}

TEST(Common_Packet, Random_Serialize_Deserialize)
{
    enum : std::size_t
    {
        ITERATIONS = 100,
    };

    using word_t = std::remove_reference_t<decltype(*::get_packet_words(nullptr))>;

    std::random_device rd {};
    std::default_random_engine re {rd()};
    std::uniform_int_distribution<word_t> word_dist {};
    std::uniform_int_distribution<std::size_t> word_count_dist {PACKET_MIN_WORDS_COUNT, PACKET_MAX_WORDS_COUNT};

    for (std::size_t i = 0; i < ITERATIONS; ++i)
    {
        const unsigned packet_number {word_dist(re)};
        std::vector<word_t> words(word_count_dist(re), 0);
        std::generate(words.begin(), words.end(), [&word_dist, &re] {
            return word_dist(re);
        });

        common::packet src {packet_number, words.size()};
        ASSERT_TRUE(src);
        std::copy(words.begin(), words.end(), src.words());
        EXPECT_EQ(src.packet_number(), packet_number);
        EXPECT_EQ(src.words_count(), words.size());

        std::vector<char> data;
        {
            const auto* sdata {static_cast<const char*>(src.data())};
            data.assign(sdata, std::next(sdata, src.data_size()));
        }
        EXPECT_GT(data.size(), 0);
        EXPECT_EQ(src.receive_timestamp(), 0);
        EXPECT_NEAR(src.send_timestamp(), ::get_milliseconds_time(), 100);

        { // Valid
            const common::packet dst {data.data(), data.size()};
            ASSERT_TRUE(dst);
            EXPECT_EQ(dst.packet_number(), packet_number);
            EXPECT_EQ(dst.words_count(), words.size());
            EXPECT_EQ(dst.send_timestamp(), src.send_timestamp());
            EXPECT_NEAR(dst.receive_timestamp(), ::get_milliseconds_time(), 100);
            EXPECT_TRUE(dst.is_read_only());
            EXPECT_TRUE(dst.is_checksum_valid());

            for (std::size_t i = 0; i < words.size(); ++i)
            {
                EXPECT_EQ(dst.word(i), words[i]);
            }
        }

        if (words.size() > PACKET_MIN_WORDS_COUNT)
        { // Size valid, but does not match
            const common::packet dst {data.data(), data.size() - 2};
            ASSERT_TRUE(dst);
            EXPECT_EQ(dst.packet_number(), packet_number);
            EXPECT_EQ(dst.words_count(), words.size() - 1);
            EXPECT_EQ(dst.send_timestamp(), src.send_timestamp());
            EXPECT_NEAR(dst.receive_timestamp(), ::get_milliseconds_time(), 100);
            EXPECT_TRUE(dst.is_read_only());
            EXPECT_FALSE(dst.is_checksum_valid());

            for (std::size_t i = 0; i < (words.size() - 1); ++i)
            {
                EXPECT_EQ(dst.word(i), words[i]);
            }
        }

        { // Checksum is invalid
            data[data.size() - 1] ^= 0xFF;
            const common::packet dst {data.data(), data.size()};
            ASSERT_TRUE(dst);
            EXPECT_EQ(dst.packet_number(), packet_number);
            EXPECT_EQ(dst.words_count(), words.size());
            EXPECT_EQ(dst.send_timestamp(), src.send_timestamp());
            EXPECT_NEAR(dst.receive_timestamp(), ::get_milliseconds_time(), 100);
            EXPECT_TRUE(dst.is_read_only());
            EXPECT_FALSE(dst.is_checksum_valid());

            for (std::size_t i = 0; i < words.size() - 1; ++i)
            {
                EXPECT_EQ(dst.word(i), words[i]);
            }
        }
    }
}
