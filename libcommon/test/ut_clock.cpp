﻿#include <gtest/gtest.h>

#include <ctime>

#include <thread>
#include <type_traits>

#include <common/clock.h>

TEST(Common_Clock, CheckWithSimpleTime)
{
    using signed_time_t = std::make_signed_t<milliseconds_time_t>;

    auto t1{static_cast<signed_time_t>(get_milliseconds_time())};
    auto t2{static_cast<signed_time_t>(std::time(nullptr) * 1000)};
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    auto t3{static_cast<signed_time_t>(get_milliseconds_time())};

    EXPECT_NEAR(t1, t2, 1000);
    EXPECT_GE(t3, t1 + 200);
}
