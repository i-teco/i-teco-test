#ifndef ITECO_LIBCOMMON_MD5_H
#define ITECO_LIBCOMMON_MD5_H

#ifdef __cplusplus
#include <cstddef> // size_t
extern "C"
{
#else
#include <stddef.h> // size_t
#endif

#define MD5_SIZE 16u

    void md5_from_block(void* digest, const void* data, size_t size);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // ITECO_LIBCOMMON_MD5_H
