#ifndef ITECO_LIBCOMMON_PROCESSOR_HPP
#define ITECO_LIBCOMMON_PROCESSOR_HPP

#include <condition_variable>
#include <functional>
#include <mutex>
#include <thread>
#include <vector>

#include <common/packet.hpp>

namespace common
{
class processor
{
public:
    using handler_t = std::function<void(const common::packet&)>;

public:
    processor(std::size_t queue_size, handler_t handler);
    ~processor();

public:
    void start();
    void stop();
    void enqueue(common::packet packet, bool force);

private:
    void pthread_proc();
    void stop_thread();

private:
    const handler_t handler_;
    std::vector<common::packet> queue_;
    std::mutex queue_mutex_;
    std::condition_variable queue_cond_;
    uint64_t write_index_;
    uint64_t read_index_;
    std::thread thread_;
};
} // namespace common
#endif // ITECO_LIBCOMMON_PROCESSOR_HPP
