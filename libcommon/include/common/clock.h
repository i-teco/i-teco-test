#ifndef ITECO_LIBCOMMON_CLOCK_H
#define ITECO_LIBCOMMON_CLOCK_H

#ifdef __cplusplus
#include <cstdint> // int16_t etc.
extern "C"
{
#else
#include <stdint.h> // int16_t etc.
#endif

#define MILLISECONDS_BUFFER_SIZE 24    // 2022/01/04 17:04:18.900 (plus zero)

    typedef uint64_t milliseconds_time_t;

    milliseconds_time_t get_milliseconds_time();
    char* milliseconds_to_string(char *buffer, milliseconds_time_t t);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // ITECO_LIBCOMMON_CLOCK_H
