#ifndef ITECO_LIBCOMMON_LOUT_HPP
#define ITECO_LIBCOMMON_LOUT_HPP

#include <iostream>
#include <mutex>

namespace common
{
class lout
{
public:
    lout()
    {
        mutex_.lock();
    }

    ~lout()
    {
        mutex_.unlock();
    }

    lout(const lout&) = delete;
    lout(lout&&) = delete;
    lout& operator=(const lout&) = delete;
    lout& operator=(lout&&) = delete;

public:
    template <typename T>
    lout& operator<<(const T& v)
    {
        std::cout << v;
        return *this;
    }

    lout& operator<<(std::ostream& (*func)(std::ostream&))
    {
        std::cout << func;
        return *this;
    }

private:
    static inline std::mutex mutex_;
};
} // namespace common
#endif // ITECO_LIBCOMMON_LOUT_HPP
