#ifndef ITECO_LIBCOMMON_PACKET_HPP
#define ITECO_LIBCOMMON_PACKET_HPP

#include <memory>

#include <common/packet.h>

namespace common
{
class packet final
{
public:
    packet(const packet&) = delete;
    packet(packet&&) = default;
    packet& operator=(const packet&) = delete;
    packet& operator=(packet&&) = default;
    ~packet() = default;

public:
    packet()
        : buffer_ {nullptr, ::packet_free}
    {
    }

    packet(unsigned packet_number, std::size_t words_count)
        : buffer_ {packet_create(packet_number, words_count), ::packet_free}
    {
    }

    packet(const void* data, size_t size)
        : buffer_ {packet_load(data, size), ::packet_free}
    {
    }

    packet(nullptr_t, std::size_t) = delete;

public:
    operator bool() const
    {
        return static_cast<bool>(buffer_);
    }

    auto packet_number() const
    {
        return ::get_packet_packet_number(buffer_.get());
    }

    auto words_count() const
    {
        return ::get_packet_words_count(buffer_.get());
    }

    auto* words()
    {
        return ::get_packet_words(buffer_.get());
    }

    const auto* words() const
    {
        return ::get_packet_words_const(buffer_.get());
    }

    auto& word(std::size_t index)
    {
        return ::get_packet_words(buffer_.get())[index];
    }

    std::remove_reference_t<decltype(*::get_packet_words(nullptr))> word(std::size_t index) const
    {
        return ::get_packet_words_const(buffer_.get())[index];
    }

    auto send_timestamp() const
    {
        return ::get_packet_send_timestamp(buffer_.get());
    }

    auto receive_timestamp() const
    {
        return ::get_packet_receive_timestamp(buffer_.get());
    }

    bool is_read_only() const
    {
        return (0 != ::packet_is_read_only(buffer_.get()));
    }

    bool is_checksum_valid() const
    {
        return (0 != ::packet_is_checksum_valid(buffer_.get()));
    }

    auto* data()
    {
        return ::get_packet_data(buffer_.get());
    }

    auto data_size() const
    {
        return ::get_packet_data_size(buffer_.get());
    }

private:
    std::unique_ptr<struct ::packet_t, void (*)(struct ::packet_t*)> buffer_;
};
} // namespace common

#endif // ITECO_LIBCOMMON_PACKET_HPP
