#ifndef ITECO_LIBCOMMON_TICKER_HPP
#define ITECO_LIBCOMMON_TICKER_HPP

#include <thread>
#include <chrono>

namespace common
{
class ticker
{
public:
    using clock = std::chrono::steady_clock;
    using time_point = typename clock::time_point;

public:
    void wait_next_time_point(uint64_t offset)
    {
        if (time_point::min() == current_tp_)
        {
            current_tp_ = clock::now();
            current_jitter_ = 0;
        }
        else
        {
            using namespace std::chrono;
            current_tp_ += milliseconds(offset);
            std::this_thread::sleep_until(current_tp_);
            current_jitter_ = duration_cast<duration<int, std::milli>>(clock::now() - current_tp_).count();
        }
    }

    auto current_jitter() const noexcept
    {
        return current_jitter_;
    }

private:
    time_point current_tp_ {time_point::min()};
    int current_jitter_ {0};
};
} // namespace common

#endif // ITECO_LIBCOMMON_TICKER_HPP
