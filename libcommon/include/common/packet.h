#ifndef ITECO_LIBCOMMON_PACKET_H
#define ITECO_LIBCOMMON_PACKET_H

#include "common/clock.h"

#ifdef __cplusplus
#include <cstddef> // size_t
#include <cstdint> // int16_t etc.
extern "C"
{
#else
#include <stddef.h> // size_t
#include <stdint.h> // int16_t etc.
#endif

#define PACKET_MAX_PACKET_NUMBER 0xFFFFu
#define PACKET_MIN_WORDS_COUNT 600u
#define PACKET_MAX_WORDS_COUNT 1600u

    struct packet_t;
    struct packet_t* packet_create(unsigned packet_number, size_t words_count);
    struct packet_t* packet_load(const void* data, size_t size);
    void packet_free(struct packet_t* packet);

    unsigned get_packet_packet_number(const struct packet_t* packet);
    size_t get_packet_words_count(const struct packet_t* packet);
    uint16_t* get_packet_words(struct packet_t* packet);
    const uint16_t* get_packet_words_const(const struct packet_t* packet);

    milliseconds_time_t get_packet_send_timestamp(const struct packet_t* packet);
    milliseconds_time_t get_packet_receive_timestamp(const struct packet_t* packet);

    int packet_is_read_only(const struct packet_t* packet);
    int packet_is_checksum_valid(const struct packet_t* packet);

    const void* get_packet_data(struct packet_t* packet);
    size_t get_packet_data_size(const struct packet_t* packet);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // ITECO_LIBCOMMON_PACKET_H
